/*
 * Copyright 2018 Foolin.  All rights reserved.
 *
 * Use of this source code is governed by a MIT style
 * license that can be found in the LICENSE file.
 *
 */

package main

import (
	"os"

	log "github.com/sirupsen/logrus"

	"gitlab.com/ulrich-schlueter/learngo/pkg/mandel"
	"gitlab.com/ulrich-schlueter/learngo/pkg/redisutils"
)

// appConfig collects all external config parameter
type appConfig struct {
	staticFolder    string
	imageFolder     string
	instanceName    string
	redisConnection string
	redispassword   string
	connection      redisutils.RedisConnection
}

func createPictureInFile(m mandel.BoundingBox, p mandel.PictureDetails, fullpath string) {

	tmpFile := fullpath + ".tmp"

	f, err := os.Create(tmpFile)
	if err != nil {
		log.Fatal(err)
	}
	log.Info("File Created. ", tmpFile)
	mandel.GeneratePicture(m, p, f)

	if err := f.Close(); err != nil {
		log.Fatal(err)
	}
	log.Info("Moving tmp file out of the way. ")
	err = os.Remove(fullpath)
	if err != nil {
		log.Error(err)
	}

	log.Info("Moving final file in the way. ")
	err = os.Rename(tmpFile, fullpath)
	if err != nil {
		log.Error(err)
	}

	log.Info("Pic ready.")
	return
}

var ac appConfig = appConfig{staticFolder: "static", imageFolder: "images", instanceName: "", redisConnection: "", redispassword: ""}

func main() {

	val, present := os.LookupEnv("MANDEL_IMAGEPATH")
	if present == true {
		ac.imageFolder = val
	}

	val, present = os.LookupEnv("HOSTNAME")
	if present == true {
		ac.instanceName = val
	}

	val, present = os.LookupEnv("REDISCONNECTION")
	if present == true {
		ac.redisConnection = val
	}

	val, present = os.LookupEnv("REDISPASSWORD")
	if present == true {
		ac.redispassword = val
	}

	if ac.redisConnection != "" {
		c, err := redisutils.CreateRedisClient(ac.redisConnection, ac.redispassword)
		if err != nil {
			log.Fatal("Failed to connect to Redis DB: %s", err.Error())
		} else {
			ac.connection = c
			log.Info("Connected to Redis at: ", ac.redisConnection)
		}
	}

	for {
		log.Info("Waiting for work")
		m, p, err := redisutils.GetItemFromQueue(ac.connection)
		if err != nil {
			log.Error("Error picking task from queue", err.Error())
		} else {
			log.Info("Got something to do ")
			log.Info("  %-v", m)
			log.Info("  %-v", p)

			createPictureInFile(m, p, p.FullPath)
			err = redisutils.PublishMessage(ac.connection, p.FullPath)
			if err != nil {
				log.Error("Error picking task from queue", err.Error())
			}
		}

	}

}
