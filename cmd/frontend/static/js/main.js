jQuery(function ($) {
  var loc = window.location;
  var uri = 'ws:';

  if (loc.protocol === 'https:') {
    uri = 'wss:';
  }
  uri += '//' + loc.host;

  ws = new WebSocket(uri + "/indexws")

  ws.onopen = function () {
    console.log('Connected')
  }

  ws.onmessage = function (evt) {
    var obj = JSON.parse(evt.data);

    var customXhttp = new XMLHttpRequest();
    var pic = obj.Imagename
    customXhttp.open("GET", '/spasingle?pic=' + pic, true);
    customXhttp.send();
    customXhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        $("#SPASINGLE").html(this.responseText);
        $("#SPAEXAMPLES").html("");
        $("#SPACUSTOM").html("");
        postSinglePictureLoad()

      }
    };


  }


  function postSinglePictureLoad() {



    $("#coords").on("submit", function (event) {
      event.preventDefault();

      var formValues = $(this).serialize();

      $.post("zoom", formValues, function (data) {
        // Display the returned data in browser
        $("#result").html(data);
        $('#imagebox').remove();

      });
    });

    $('#target').Jcrop({
      onChange: showCoords,
      onSelect: showCoords,
      onRelease: clearCoords
    }, function () {
      jcrop_api = this;
    });

  }



  $('#btn-examples').on('click', function () {
    var examplesXhttp = new XMLHttpRequest();
    examplesXhttp.open("GET", '/spaexamples', true);
    examplesXhttp.send();
    examplesXhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        $("#SPAEXAMPLES").html(this.responseText);
        $("#SPACUSTOM").html("");
        $("#SPASINGLE").html("");

        $("img[id*='pic']").on('click', function (evt) {
          var customXhttp = new XMLHttpRequest();
          var pic = $(evt.target).attr('data-pic')
          customXhttp.open("GET", '/spasingle?pic=' + pic +"&example=true", true);
          customXhttp.send();
          customXhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
              $("#SPASINGLE").html(this.responseText);
              $("#SPAEXAMPLES").html("");
              $("#SPACUSTOM").html("");
              postSinglePictureLoad()

            }
          };
        })

      }
    };
  })

  $('#btn-custom').on('click', function () {
    var customXhttp = new XMLHttpRequest();
    customXhttp.open("GET", '/spacustom', true);
    customXhttp.send();
    customXhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        $("#SPACUSTOM").html(this.responseText);
        $("#SPAEXAMPLES").html("");
        $("#SPASINGLE").html("");

        $("img[id*='pic']").on('click', function (evt) {
          var customXhttp = new XMLHttpRequest();
          var pic = $(evt.target).attr('data-pic')
          customXhttp.open("GET", '/spasingle?pic=' + pic, true);
          customXhttp.send();
          customXhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
              $("#SPASINGLE").html(this.responseText);
              $("#SPAEXAMPLES").html("");
              $("#SPACUSTOM").html("");
              postSinglePictureLoad()

            }
          };
        })

      }
    };
  })





});




var elements = document.getElementsByClassName("column");

// Declare a loop variable
var i;

// Full-width images
function one() {
  for (i = 0; i < elements.length; i++) {
    elements[i].style.msFlex = "100%";  // IE10
    elements[i].style.flex = "100%";
  }
}

// Two images side by side
function two() {
  for (i = 0; i < elements.length; i++) {
    elements[i].style.msFlex = "50%";  // IE10
    elements[i].style.flex = "50%";
  }
}

// Four images side by side
function four() {
  for (i = 0; i < elements.length; i++) {
    elements[i].style.msFlex = "25%";  // IE10
    elements[i].style.flex = "25%";
  }
}

// Add active class to the current button (highlight it)
var header = document.getElementById("myHeader");
var btns = header.getElementsByClassName("btn");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function () {
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}


function submitform() {
  var form = $('#coords')
  if (form.onsubmit &&
    !form.onsubmit()) {
    return;
  }
  form.submit();
}
