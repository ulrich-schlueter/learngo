/*
 * Copyright 2018 Foolin.  All rights reserved.
 *
 * Use of this source code is governed by a MIT style
 * license that can be found in the LICENSE file.
 *
 */

package main

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"sort"
	"strings"

	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
	"k8s.io/apimachinery/pkg/util/json"

	"github.com/foolin/goview/supports/echoview-v4"
	"github.com/gorilla/websocket"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"gitlab.com/ulrich-schlueter/learngo/pkg/mandel"
	"gitlab.com/ulrich-schlueter/learngo/pkg/redisutils"
)

var (
	upgrader = websocket.Upgrader{}
	ws       *websocket.Conn
)

// appConfig collects all external config parameter
type appConfig struct {
	staticFolder    string
	imageFolder     string
	examplesFolder  string
	instanceName    string
	redisConnection string
	redispassword   string
	connection      redisutils.RedisConnection
}

// appConfig collects all external config parameter
type picMessage struct {
	Imagename  string
	MandelInfo mandel.BoundingBox
}

func reverse(entries []string) []string {
	for i := 0; i < len(entries)/2; i++ {
		j := len(entries) - i - 1
		entries[i], entries[j] = entries[j], entries[i]
	}
	return entries
}

func getListOfImages(path string) []string {

	mfiles, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}
	sort.Slice(mfiles, func(i, j int) bool {
		return mfiles[i].ModTime().Unix() < mfiles[j].ModTime().Unix()
	})

	filenames := []string{}
	for _, m := range mfiles {
		if strings.HasSuffix(m.Name(), ".png") {
			x := filepath.Base(m.Name())
			filenames = append(filenames, x)
		}

	}

	//fmt.Println(filenames) // contains a list of all files in the current directory
	return reverse(filenames)
}

func generateImageFilename() (filename string, fullpath string) {
	const layout = "01-02-2006-15-04-05"
	t := time.Now()
	filename = ac.instanceName + "-image-" + t.Format(layout) + ".png"
	fullpath = ac.imageFolder + string(os.PathSeparator) + filename
	return
}

func createPictureInFile(m mandel.BoundingBox, p mandel.PictureDetails, fullpath string) {

	f, err := os.Create(fullpath)
	if err != nil {
		log.Fatal(err)
	}

	mandel.GeneratePicture(m, p, f)

	if err := f.Close(); err != nil {
		log.Fatal(err)
	}
	return
}

func storeImageSpecs(mand mandel.BoundingBox, p mandel.PictureDetails) {
	jsonString, err := json.Marshal(mand)
	if err != nil {
		log.Error(err.Error())
	} else {
		log.Info(string(jsonString), p.FullPath+".json")
		f, err := os.Create(p.FullPath + ".json")

		if err != nil {
			log.Fatal(err)
		}

		defer f.Close()

		_, err = f.WriteString(string(jsonString))
	}
}

func getImageSpecs(p mandel.PictureDetails) (mand mandel.BoundingBox) {
	fileName := p.FullPath + ".json"

	_, err := os.Stat(fileName)
	if os.IsNotExist(err) {
		return
	}

	content, err := ioutil.ReadFile(fileName)
	if err != nil {
		log.Error(err)
	}

	err = json.Unmarshal([]byte(content), &mand)
	if err != nil {
		panic(err)
	}

	// Convert []byte to string and print to screen

	return
}

func zoomIn(c echo.Context) error {
	mand := defaultPic
	filename, fullpath := generateImageFilename()
	p := mandel.PictureDetails{Width: 1024, Height: 1024, Name: filename, FullPath: fullpath}

	method := c.Request().Method
	if method == http.MethodGet {
		fmt.Print("Dd")
	} else {
		//Must be POST

		x1 := c.FormValue("x1")
		x2 := c.FormValue("x2")
		y1 := c.FormValue("y1")
		y2 := c.FormValue("y2")
		iterations := c.FormValue("iterations")

		x1f, err := strconv.ParseFloat(x1, 64)
		x2f, err := strconv.ParseFloat(x2, 64)
		y1f, err := strconv.ParseFloat(y1, 64)
		y2f, err := strconv.ParseFloat(y2, 64)
		iterationsI, err := strconv.Atoi(iterations)
		if err != nil {
			log.Error(err)
		} else {
			mand = mandel.BoundingBox{MinX: x1f, MaxX: x2f, MinY: y1f, MaxY: y2f, Iterations: iterationsI}
			log.Info(mand)
		}
	}

	go genPic(mand, p, filename)

	message := "Generating picture " + filename + ". Please wait a few seconds."
	return c.Render(http.StatusOK, "spa-confirm.html", echo.Map{"message": message})

}

func genPic(mand mandel.BoundingBox, p mandel.PictureDetails, filename string) {
	storeImageSpecs(mand, p)
	if ac.redisConnection != "" {
		log.Info("trying to create task item on  Redis queue ")

		err := redisutils.AddItemToQueue(ac.connection, mand, p)
		if err != nil {
			log.Info("Failed  ", err.Error())
		} else {
			log.Info("Worked. ")
			for {
				log.Info("Waiting ")
				finished, err := redisutils.WaitForMessage(ac.connection)
				if err != nil {
					log.Info("Failed to get message ", err.Error())
				}
				log.Info("Got message ", finished)
				if finished == p.FullPath {
					break
				}
			}

		}
	} else {
		createPictureInFile(mand, p, p.FullPath)
	}
	message := picMessage{Imagename: filename, MandelInfo: mand}
	messageJSON, err := json.Marshal(message)
	err = ws.WriteMessage(websocket.TextMessage, messageJSON)
	if err != nil {
		log.Error(err)
	}
}

func registerWS(c echo.Context) error {
	ws2, err := upgrader.Upgrade(c.Response(), c.Request(), nil)
	ws = ws2
	if err != nil {
		fmt.Printf("%s\n", err.Error())
		//return err
	}
	defer ws.Close()

	for {

		// Read
		_, msg, err := ws.ReadMessage()
		if err != nil {
			c.Logger().Error(err)
		}
		fmt.Printf("%s\n", msg)
	}
}

//*****************************************************************************************

func unescape(s string) template.HTML {
	return template.HTML(s)
}

func getHTMLForFiles(folder string) (html string) {
	h := ""
	files := getListOfImages(folder)
	h += "<div id=\"uli\">"
	h += "<div class=\"column\">"
	for i, f := range files {
		id := "pic" + strconv.Itoa(i)
		h += "<img data-pic=\"" + f + "\" id=\"" + id + "\" src=\"/" + folder + "/" + f + "\" style=\"width:25%\">"
	}
	h += "</div></div>"

	html = h
	return
}

var ac appConfig = appConfig{staticFolder: "static", imageFolder: "images", examplesFolder: "static/examples", instanceName: "", redisConnection: "", redispassword: ""}
var defaultPic mandel.BoundingBox = mandel.BoundingBox{MinX: -2.5, MaxX: 1, MinY: -1, MaxY: 1, Iterations: 1000}

func main() {

	// /log.SetFormatter(&log.JSONFormatter{})

	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	val, present := os.LookupEnv("MANDEL_IMAGEPATH")
	if present == true {
		ac.imageFolder = val
	}

	val, present = os.LookupEnv("HOSTNAME")
	if present == true {
		ac.instanceName = val
	}

	val, present = os.LookupEnv("REDISCONNECTION")
	if present == true {
		ac.redisConnection = val
	}

	val, present = os.LookupEnv("REDISPASSWORD")
	if present == true {
		ac.redispassword = val
	}

	if ac.redisConnection != "" {
		c, err := redisutils.CreateRedisClient(ac.redisConnection, ac.redispassword)
		if err != nil {
			log.Fatal("Failed to connect to Redis DB: %s", err.Error())
		} else {
			ac.connection = c
			log.Info("Connected to Redis at: ", ac.redisConnection)
		}
	}

	//Set Renderer

	e.Renderer = echoview.Default()

	e.Static("/static", ac.staticFolder)
	e.Static("/images", ac.imageFolder)

	log.Info("Static:", ac.staticFolder)
	log.Info("Images:", ac.imageFolder)
	log.Info("Examples:", ac.examplesFolder)

	// Routes
	e.GET("/", func(c echo.Context) error {
		return c.Render(http.StatusOK, "index.html", echo.Map{})
	})

	e.GET("/spacustom", func(c echo.Context) error {
		h := getHTMLForFiles(ac.imageFolder)
		return c.Render(http.StatusOK, "spa-intern.html", echo.Map{"picsHtml": h, "unescape": unescape})
	})

	e.GET("/spaexamples", func(c echo.Context) error {
		h := getHTMLForFiles(ac.examplesFolder)
		return c.Render(http.StatusOK, "spa-examples.html", echo.Map{"picsHtml": h, "unescape": unescape})
	})

	e.GET("/spasingle", func(c echo.Context) error {
		pic := c.QueryParam("pic")
		example := c.QueryParam("example")
		folder := ac.imageFolder
		if example == "true" {
			folder = ac.examplesFolder
		}
		mand := getImageSpecs(mandel.PictureDetails{FullPath: folder + "/" + pic})
		return c.Render(http.StatusOK, "spa-single.html", echo.Map{"pic": pic, "curr": mand, "folder": folder})

	})

	e.GET("/indexws", registerWS)
	e.POST("/zoom", zoomIn)
	e.GET("/zoom", zoomIn)

	// Start server
	e.Logger.Fatal(e.Start(":9091"))
}
