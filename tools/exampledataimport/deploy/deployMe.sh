#!/bin/bash
set -x
VERSION=$1
REDISENDPOINTSERVICE=redis-master
PASSWORD=Opstree@1234

CLUSTERIP=$(oc get svc $REDISENDPOINTSERVICE --template="{{.spec.clusterIP}}")

sed "s/XX_CLUSTERIP_XX/$CLUSTERIP/g" goredis-configmap-cmdparams.yaml.template > goredis-configmap-cmdparams.yaml

sed "s/XX_VERSION_XX/$VERSION/g" goredis-job-filldata.yaml.template > goredis-job-filldata.yaml

for name in goredis-configmap-cmdparams.yaml goredis-configmap-sampledata.yaml goredis-job-filldata.yaml
do
    echo $name
    oc delete -f $name
    oc apply  -f $name
done






