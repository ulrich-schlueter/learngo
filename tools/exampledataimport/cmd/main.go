package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"

	"gitlab.com/ulrich-schlueter/learngo/pkg/mandel"
	"gitlab.com/ulrich-schlueter/learngo/pkg/redisutils"
)

var redisKeyName = "SampleParameters"

var examples = []mandel.Mandel{
	{-2.5, 1, 2, 100},
	{-0.7463, 0.1102, 0.005, 100},
	{-0.415, -0.683, 1.0 / 80.0, 1000},
	{0.28693186889504513, 0.014286693904085048, 6.349e-4, 1000},
	{-0.023130818687499735, -0.9989908810312501, 9.26420625E-5, 1000},
}

func main() {

	redisPtr := flag.String("redisconnection", "127.0.0.1:6379", "IP:Port of the redis api to use.")
	examplesPtr := flag.String("examplefile", "", "filename with sample data to use.")
	authPtr := flag.String("auth", "", "password if required.")
	flag.Parse()

	var exampleData []mandel.Mandel

	if *examplesPtr == "" {
		exampleData = examples
		fmt.Println("Examples:", "(local)")
	} else {
		jsonFile, err := os.Open(*examplesPtr)
		if err != nil {
			fmt.Println(err)
		}

		defer jsonFile.Close()

		byteValue, _ := ioutil.ReadAll(jsonFile)

		json.Unmarshal(byteValue, &exampleData)
		fmt.Println("Examples: from file \"" + *examplesPtr + "\"")
		fmt.Println(exampleData)
	}

	fmt.Println("Redis Connection:", *redisPtr)
	fmt.Println("Key:", redisKeyName)

	var client redisutils.RedisConnection

	client, err := redisutils.CreateRedisClient(*redisPtr, *authPtr)
	if err != nil {
		fmt.Printf("CreateRedisClient: " + err.Error())
	}
	length, err := redisutils.GetNumberOfExamples(client)
	if err != nil {
		fmt.Printf("GetNumberOfExamples: " + err.Error())
	}
	fmt.Println("Len: " + strconv.Itoa(length))
	err = redisutils.ClearExampleList(client)

	for _, m := range exampleData {
		redisutils.InjectExample(client, m)
		fmt.Println(m)

	}

	list, err := redisutils.GetExampleParamList(client)
	fmt.Println(list)
}
