module gitlab.com/ulrich-schlueter/learngo

go 1.14

require (
	github.com/foolin/goview v0.3.0
	github.com/google/pprof v0.0.0-20201203190320-1bf35d6f28c2 // indirect
	github.com/gorilla/websocket v1.4.2
	github.com/labstack/echo/v4 v4.1.17
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mediocregopher/radix/v3 v3.6.0
	github.com/sirupsen/logrus v1.7.0
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897 // indirect
	golang.org/x/net v0.0.0-20201031054903-ff519b6c9102 // indirect
	golang.org/x/sys v0.0.0-20201204225414-ed752295db88 // indirect
	golang.org/x/text v0.3.4 // indirect
	k8s.io/apimachinery v0.19.4

)
