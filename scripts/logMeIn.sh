#!/bin/bash

adminuser=$( crc console --credentials -o json | jq -r .clusterConfig.adminCredentials.username ) 
adminpw=$( crc console --credentials -o json | jq -r .clusterConfig.adminCredentials.password )

echo "OC:"
oc login -u $adminuser -p $adminpw  --insecure-skip-tls-verify=true https://api.crc.testing:6443
echo "PODMAN:"
podman login -u kubeadmin -p $(oc whoami -t) default-route-openshift-image-registry.apps-crc.testing