#!/bin/bash
#set -x
CRCFILESTEM="crc-linux-amd64"
CRCURL="https://mirror.openshift.com/pub/openshift-v4/clients/crc/latest"
RELEASEJSON="$CRCURL/release-info.json"
CRCPULLSECRET=~/crc-pull-secret.txt
CRC_CPUS=12
CRC_MEM=32000
defaultProject=j7first
FLAG=$1 
if [[ "${FLAG,,}" == "force" ]] ; then
   	currentversion="none"
else
	currentversion=$( crc version -o json | jq -r .version )
fi

latestversion=$( curl -s -k "$RELEASEJSON" | jq  .version | jq -r .crcVersion )
echo "$currentversion  <-> $latestversion"
if [ "$currentversion" !=  "$latestversion" ] ; then

    echo "Not latest version"
    fileExt=".tar.xz"
    downloadThis="$CRCURL/$CRCFILESTEM$fileExt"
    targetfile="$CRCFILESTEM-$latestversion$fileExt"
    targetfile="$CRCFILESTEM-$latestversion$fileExt"
    tmpFile="$targetfile.piece"

    echo "$downloadThis"
    echo "$targetfile"

    if test -f "$targetfile"; then
        echo "$targetfile already present. Skipping download"
    else
        curl -k -s -o $tmpFile $downloadThis
        res=$?
        if test "$res" != "0"; then
            echo "the curl command failed with: $res"
            exit 8
        else 
            mv $tmpFile $targetfile
        fi
    fi

    extractFolder=$( tar tvf $targetfile | grep "drw" | tr " " "\n" | tail -n1 )     
    tar -xvf $targetfile
            
    crc delete -f 

    cp $extractFolder/crc ~/bin/crc

    crc setup

    crc start -p $CRCPULLSECRET -c $CRC_CPUS -m $CRC_MEM


    adminuser=$( crc console --credentials -o json | jq -r .clusterConfig.adminCredentials.username ) 
    adminpw=$( crc console --credentials -o json | jq -r .clusterConfig.adminCredentials.password )

    oc login -u $adminuser -p $adminpw  --insecure-skip-tls-verify=true https://api.crc.testing:6443


    oc extract secret/router-ca --keys=tls.crt -n openshift-ingress-operator --confirm
    sudo mkdir -p  /etc/docker/certs.d/default-route-openshift-image-registry.apps-crc.testing/
    sudo cp tls.crt /etc/docker/certs.d/default-route-openshift-image-registry.apps-crc.testing/
    sudo chmod +r /etc/docker/certs.d/default-route-openshift-image-registry.apps-crc.testing/tls.crt
    
    oc create namespace $defaultProject
else

 	echo "No later version found. Nothing to do."

fi

