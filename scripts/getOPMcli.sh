#!/bin/bash
set -x

#https://docs.openshift.com/container-platform/4.6/cli_reference/opm-cli.html#opm-cli


./logMeIn.sh


export REG_CREDS=${XDG_RUNTIME_DIR}/containers/auth.json
podman login registry.redhat.io

#default-route-openshift-image-registry.apps-crc.testing
#docker run -it --entrypoint sh registry.redhat.io/openshift4/ose-operator-registry
#mkdir -p /tmp/opm

oc image extract registry.redhat.io/openshift4/ose-operator-registry:v4.6 \
    -a ${REG_CREDS} \
    --path /usr/bin/opm:. \
    --confirm

chmod +x ./opm

mv ./opm ~/bin

opm version