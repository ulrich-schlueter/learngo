#!/bin/bash

NAME=some-redis

docker stop $NAME

docker rm $NAME

docker run --name $NAME -p 6379:6379 -d redis