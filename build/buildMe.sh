#!/bin/bash
set -x
VERSION=$1
FRONTEND_IMAGENAME=learngo-fe
BACKEND_IMAGENAME=learngo-be

#Frontend
#podman build -f ../Dockerfile.frontend -t $FRONTEND_IMAGENAME:$VERSION --no-cache
podman build -f ../Dockerfile.frontend -t $FRONTEND_IMAGENAME:$VERSION 
feimageid=$(podman images localhost/$FRONTEND_IMAGENAME:$VERSION --format  "{{.Id}}" | tail -n 1)


podman login -u kubeadmin -p $(oc whoami -t) default-route-openshift-image-registry.apps-crc.testing
podman push  $feimageid docker://default-route-openshift-image-registry.apps-crc.testing/j7first/$FRONTEND_IMAGENAME:$VERSION


#Backend
podman build -f ../Dockerfile.backend -t $BACKEND_IMAGENAME:$VERSION 
beimageid=$(podman images localhost/$BACKEND_IMAGENAME:$VERSION --format  "{{.Id}}" | tail -n 1)


podman login -u kubeadmin -p $(oc whoami -t) default-route-openshift-image-registry.apps-crc.testing
podman push  $beimageid docker://default-route-openshift-image-registry.apps-crc.testing/j7first/$BACKEND_IMAGENAME:$VERSION



