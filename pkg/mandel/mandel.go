package mandel

import (
	"image"
	"image/color"
	"image/png"
	"io"
	"log"
	"math/big"
	"sync"

	col "gitlab.com/ulrich-schlueter/learngo/pkg/color"
)

//Prec ...
const Prec = 200

// Mandel brot
type Mandel struct {
	CenterX    float64
	CenterY    float64
	Radius     float64
	Iterations int
}

//BoundingBox ...
type BoundingBox struct {
	MinX       float64
	MinY       float64
	MaxX       float64
	MaxY       float64
	Iterations int
}

//MandelBig brot
type MandelBig struct {
	CenterX    *big.Float
	CenterY    *big.Float
	Radius     *big.Float
	Iterations int64
}

//BoundingBoxBig ...
type BoundingBoxBig struct {
	MinX       *big.Float
	MinY       *big.Float
	MaxX       *big.Float
	MaxY       *big.Float
	Iterations int64
}

//PictureDetails ..
type PictureDetails struct {
	Width    int
	Height   int
	Name     string
	FullPath string
}

// ToBoundingBox ...
func ToBoundingBox(m Mandel) (b BoundingBox) {
	b.MinX = m.CenterX - m.Radius
	b.MinY = m.CenterY - m.Radius

	b.MaxX = m.CenterX + m.Radius
	b.MaxY = m.CenterY + m.Radius
	b.Iterations = m.Iterations
	return
}

// ToBoundingBoxBig ...
func ToBoundingBoxBig(m MandelBig) (b BoundingBoxBig) {
	b.MinX.Sub(m.CenterX, m.Radius)
	b.MinY.Sub(m.CenterY, m.Radius)

	b.MaxX.Add(m.CenterX, m.Radius)
	b.MaxY.Add(m.CenterY, m.Radius)
	b.Iterations = m.Iterations
	return
}

// iterate for a single point in the mandelbrot set, with a limit
func calc(px float64, py float64, maxIterations int) int {
	x0 := px
	y0 := py
	x := 0.0
	y := 0.0
	xx := 0.0
	yy := 0.0
	var iter int = 0

	for (xx+yy <= 2*2) && (iter < maxIterations) {
		xx = x * x
		yy = y * y
		xtemp := xx - yy + x0
		y = 2*x*y + y0
		x = xtemp
		iter++
	}
	return iter
}

//ConvF ..
func ConvF(f float64) (b *big.Float) {

	b = new(big.Float).SetPrec(Prec).SetFloat64(f)
	return
}

//ConvI ..
func ConvI(i int64) (b *big.Float) {

	b = new(big.Float).SetPrec(Prec).SetInt64(i)
	return
}

// iterate for a single point in the mandelbrot set, with a limit
func calcBig(px *big.Float, py *big.Float, maxIterations int64) int64 {
	x0 := px
	y0 := py
	x := new(big.Float).SetPrec(Prec).SetFloat64(0)
	y := new(big.Float).SetPrec(Prec).SetFloat64(0)
	four := new(big.Float).SetPrec(Prec).SetFloat64(4)
	two := new(big.Float).SetPrec(Prec).SetFloat64(2)
	xx := new(big.Float).SetPrec(Prec).SetFloat64(0)
	yy := new(big.Float).SetPrec(Prec).SetFloat64(0)
	xxyysum := new(big.Float)
	xtemp := new(big.Float)
	xxyysum.Add(xx, yy)
	var iter int64 = 0

	for (xxyysum.Cmp(four) == -1) && (iter < maxIterations) {
		xx.Mul(x, x)
		yy.Mul(y, y)
		xtemp.Sub(xx, yy)
		xtemp.Add(xtemp, x0)
		y.Mul(x, y)
		y.Mul(y, two)
		y.Add(y, y0)

		x.Copy(xtemp)
		iter++
		xxyysum.Add(xx, yy)
	}
	return iter
}

// GeneratePicture The big loop over the whole picture
func GeneratePicture(mand BoundingBox, pictureDetails PictureDetails, w io.Writer) {
	const width, height = 1024, 1024

	// Create a colored image of the given width and height.
	img := image.NewNRGBA(image.Rect(0, 0, width, height))
	iterationsArray := make([][]int, width)
	for i := range iterationsArray {
		iterationsArray[i] = make([]int, height)
	}
	histCount := make(map[int]int)

	var wg sync.WaitGroup
	for y := 0; y < height; y++ {
		wg.Add(1)
		go func(y int) {
			defer wg.Done()
			for x := 0; x < width; x++ {
				px := (float64(x) * (mand.MaxX - mand.MinX) / width) + mand.MinX
				py := (float64(y) * (mand.MaxY - mand.MinY) / height) + mand.MinY
				iterations := calc(px, py, mand.Iterations)
				iterationsArray[x][y] = iterations

			}
		}(y)
	}
	wg.Wait()

	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			i := iterationsArray[x][y]
			histCount[i]++
		}
	}

	var (
		total int = 0
		i     int = 0
	)
	for i = 0; i < mand.Iterations; i++ {
		total += histCount[i]
	}
	for y := 0; y < height; y++ {
		wg.Add(1)
		go func(y int) {
			defer wg.Done()
			for x := 0; x < width; x++ {
				iters := iterationsArray[x][y]
				h := 0.0
				for key, value := range histCount {
					if key <= iters {

						h += float64(value) / float64(total)
					}
				}
				//hue := (float64(iterations)) / float64((mand.maxIterations))
				hu := col.HSL{H: h, S: 1, L: 0.5}
				h1 := col.ToRGB(hu)
				img.Set(x, y, color.NRGBA{
					R: uint8(h1.R * 255),
					G: uint8(h1.G * 255),
					B: uint8(h1.B * 255),
					A: 255,
				})
			}
		}(y)
	}
	wg.Wait()

	if err := png.Encode(w, img); err != nil {
		log.Fatal(err)
	}

}

// GeneratePicture The big loop over the whole picture
func GeneratePictureBig(mand BoundingBoxBig, pictureDetails PictureDetails, w io.Writer) {
	const width, height = 1024, 1024

	// Create a colored image of the given width and height.
	img := image.NewNRGBA(image.Rect(0, 0, width, height))
	iterationsArray := make([][]int64, width)
	for i := range iterationsArray {
		iterationsArray[i] = make([]int64, height)
	}
	histCount := make(map[int64]int64)

	var wg sync.WaitGroup
	for y := int64(0); y < height; y++ {
		wg.Add(1)
		go func(y int64) {
			defer wg.Done()
			for x := int64(0); x < width; x++ {
				px := new(big.Float)
				py := new(big.Float)
				//px = (float64(x) * (mand.MaxX - mand.MinX) / width) + mand.MinX
				px.Sub(mand.MaxX, mand.MinX)
				px.Mul(px, ConvI(x))
				px.Quo(px, ConvI(width))
				px.Add(px, mand.MinX)

				py.Sub(mand.MaxY, mand.MinY)
				py.Mul(py, ConvI(y))
				py.Quo(py, ConvI(height))
				py.Add(py, mand.MinY)

				iterations := calcBig(px, py, mand.Iterations)
				iterationsArray[x][y] = iterations

			}
		}(y)
	}
	wg.Wait()

	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			i := iterationsArray[x][y]
			histCount[i]++
		}
	}

	var (
		total int64 = 0
		i     int64 = 0
	)
	for i = 0; i < mand.Iterations; i++ {
		total += histCount[i]
	}
	for y := 0; y < height; y++ {
		wg.Add(1)
		go func(y int) {
			defer wg.Done()
			for x := 0; x < width; x++ {
				iters := iterationsArray[x][y]
				h := 0.0
				for key, value := range histCount {
					if key <= iters {

						h += float64(value) / float64(total)
					}
				}
				//hue := (float64(iterations)) / float64((mand.maxIterations))
				hu := col.HSL{H: h, S: 1, L: 0.5}
				h1 := col.ToRGB(hu)
				img.Set(x, y, color.NRGBA{
					R: uint8(h1.R * 255),
					G: uint8(h1.G * 255),
					B: uint8(h1.B * 255),
					A: 255,
				})
			}
		}(y)
	}
	wg.Wait()

	if err := png.Encode(w, img); err != nil {
		log.Fatal(err)
	}

}
