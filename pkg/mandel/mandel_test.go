package mandel

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"math/big"
	"testing"
)

// TestCalc tests the calculation logic of a single point
func TestCalc(t *testing.T) {
	var maxIterations int = 100
	tables := []struct {
		x float64
		y float64
		n int
	}{
		{0.0, 0.0, maxIterations},
		{1.0, 1.0, 3},
		{-1.401155, 1.0397, 4},
	}

	for _, table := range tables {

		iterations := calc(table.x, table.y, maxIterations)
		if iterations != table.n {
			t.Errorf("Iterations for (%f,%f) was incorrect, got: %d, want: %d.", table.x, table.y, iterations, table.n)
		}
	}

}

func TestPic(t *testing.T) {

	m := BoundingBox{-2.5, -1, 1, 1, 100}
	p := PictureDetails{Width: 1024, Height: 1024}
	var buf bytes.Buffer
	expected := "3ec68af4d7d0f35140b0d33b19f235c0"

	GeneratePicture(m, p, &buf)
	h := md5.New()
	h.Write(buf.Bytes())
	s := hex.EncodeToString(h.Sum(nil))

	if s != expected {
		t.Errorf("got: %s, want: %s", s, expected)
	}
	//io.WriteString(h, &buf)

}

// TestCalc tests the calculation logic of a single point
func TestCalcBig(t *testing.T) {
	var maxIterations int64 = 100
	tables := []struct {
		x *big.Float
		y *big.Float
		n int64
	}{
		{ConvF(0.0), ConvF(0.0), maxIterations},
		{ConvF(1.0), ConvF(1.0), 3},
		{ConvF(-1.401155), ConvF(1.0397), 4},
	}

	for _, table := range tables {

		iterations := calcBig(table.x, table.y, maxIterations)
		if iterations != table.n {
			t.Errorf("Iterations for (%f,%f) was incorrect, got: %d, want: %d.", table.x, table.y, iterations, table.n)
		}
	}

}

func TestGenPicBig(t *testing.T) {

	var buf bytes.Buffer
	expected := "3ec68af4d7d0f35140b0d33b19f235c0"

	m := BoundingBoxBig{ConvF(-2.5), ConvF(-1), ConvF(1), ConvF(1), 100}
	p := PictureDetails{Width: 1024, Height: 1024}
	GeneratePictureBig(m, p, &buf)
	h := md5.New()
	h.Write(buf.Bytes())
	s := hex.EncodeToString(h.Sum(nil))
	if s != expected {
		t.Errorf("got: %s, want: %s", s, expected)
	}

	return
}
