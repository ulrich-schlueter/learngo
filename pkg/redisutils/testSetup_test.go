package redisutils

import "os"

func testSetup() (conn RedisConnection, err error) {

	password, present := os.LookupEnv("REDISPASSWORD")
	if present == false {

	}

	conn, err = CreateRedisClient("127.0.0.1:6379", password)

	return
}
