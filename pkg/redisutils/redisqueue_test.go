package redisutils

import (
	"testing"

	"gitlab.com/ulrich-schlueter/learngo/pkg/mandel"
)

func TestQueueDequeue(t *testing.T) {
	client, err := testSetup()

	if err != nil {
		t.Errorf("Failed to build client: %s ", err.Error())
	}

	mIn := mandel.BoundingBox{MinX: 1, MinY: 2, MaxX: 3, MaxY: 5, Iterations: 6}
	pIn := mandel.PictureDetails{Width: 7, Height: 8, Name: "Name", FullPath: "FullPath"}

	err = ClearQueue(client)
	if err != nil {
		t.Errorf("Failed to clear queue: %s ", err.Error())
	}

	err = AddItemToQueue(client, mIn, pIn)
	if err != nil {
		t.Errorf("Failed to place Item in Queue: %s ", err.Error())
	}

	mOut, pOut, err := GetItemFromQueue(client)
	if err != nil {
		t.Errorf("Failed to Get Item From Queue: %s ", err.Error())
	}

	if mIn != mOut {
		t.Errorf("Mandel: Input different to Output: %+v vs %+v ", mIn, mOut)
	}

	if pIn != pOut {
		t.Errorf("Picture DetailsInput different to Output: %+v vs %+v ", pIn, pOut)
	}

}
