package redisutils

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	radix "github.com/mediocregopher/radix/v3"
	"gitlab.com/ulrich-schlueter/learngo/pkg/mandel"
)

//RedisConnection ...
type RedisConnection struct {
	redisClient  radix.Client
	pubsubClient radix.PubSubConn
	Name         string
}

var redisKeyName = "ExampleParameters"

//CreateRedisClient ...
func CreateRedisClient(redisConnection string, password string) (client RedisConnection, err error) {

	client = RedisConnection{}
	customConnFunc := func(network, addr string) (radix.Conn, error) {
		return nil, nil
	}

	if password != "" {
		customConnFunc = func(network, addr string) (radix.Conn, error) {
			return radix.Dial(network, addr,
				radix.DialTimeout(1*time.Minute),
				radix.DialAuthPass(password),
			)
		}
	} else {
		customConnFunc = func(network, addr string) (radix.Conn, error) {
			return radix.Dial(network, addr,
				radix.DialTimeout(1*time.Minute),
			)
		}
	}

	//get a first connection and determine if we are in a cluster or not
	pool, err := radix.NewPool("tcp", redisConnection, 10, radix.PoolConnFunc(customConnFunc))
	if err != nil {
		fmt.Println("Could Not Connect: " + err.Error())
		return

	}

	var info string
	err = pool.Do(radix.Cmd(&info, "INFO", "CLUSTER"))
	splitstring := strings.Split(info, "\r\n")
	fmt.Println("Cluster Info String: " + info)
	fmt.Println("Cluster Enabled String: " + splitstring[1])

	if splitstring[1] == "cluster_enabled:0" {
		fmt.Println("Pool Client created.")
		client.redisClient = pool
		fmt.Println("Creating Pubsub client.")

		conn, err2 := customConnFunc("tcp", redisConnection)
		if err2 != nil {
			fmt.Println("Could Not Connect pubsub client: " + err.Error())
			return
		}
		client.pubsubClient = radix.PubSub(conn)

		return
	}

	fmt.Println("Is Cluster. ")
	poolFunc := func(network, addr string) (radix.Client, error) {
		return radix.NewPool(network, addr, 100, radix.PoolConnFunc(customConnFunc))
	}

	cluster, err := radix.NewCluster([]string{redisConnection}, radix.ClusterPoolFunc(poolFunc))
	if err != nil {
		fmt.Println("Could Not Connect: " + err.Error())

	}

	conn, err2 := customConnFunc("tcp", redisConnection)

	if err2 != nil {
		fmt.Println("Could Not Connect Cluster pubsub client: " + err.Error())
		return
	}
	client.pubsubClient = radix.PubSub(conn)

	fmt.Println("Cluster Clients created.")
	client.redisClient = cluster
	return
}

//ClearExampleList Example List
func ClearExampleList(client RedisConnection) (err error) {
	err = client.redisClient.Do(radix.Cmd(nil, "DEL", redisKeyName))
	if err != nil {
		fmt.Println("Error Deleting key " + err.Error())
	}
	return
}

//GetNumberOfExamples ...
func GetNumberOfExamples(client RedisConnection) (len int, err error) {
	var lenString string
	err = client.redisClient.Do(radix.Cmd(&lenString, "LLEN", redisKeyName))
	if err != nil {
		fmt.Println("Error Accessing list " + err.Error())
		return
	}
	fmt.Println("#entries in key list: " + lenString)
	len, err = strconv.Atoi(lenString)
	return
}

// InjectExample ...
func InjectExample(client RedisConnection, m mandel.Mandel) (err error) {

	b, _ := json.Marshal(m)
	bstring := string(b)

	err = client.redisClient.Do(radix.Cmd(nil, "LPUSH", redisKeyName, bstring))

	if err != nil {
		// handle error
		fmt.Println("Could Not do something " + err.Error())

	}
	return

}

// GetExampleParamList ...
func GetExampleParamList(client RedisConnection) (list []string, err error) {

	err = client.redisClient.Do(radix.Cmd(&list, "LRANGE", redisKeyName, "0", "-1"))
	if err != nil {
		fmt.Println("Err retrieving list of examples: " + err.Error())

	}
	return

}
