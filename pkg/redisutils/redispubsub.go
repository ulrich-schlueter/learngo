package redisutils

import (
	"log"

	radix "github.com/mediocregopher/radix/v3"
	"gitlab.com/ulrich-schlueter/learngo/pkg/mandel"
)

//MessageItem ...
type MessageItem struct {
	MandelBox      mandel.BoundingBox
	PictureDetails mandel.PictureDetails
}

var redisTopic = "MandelTopic"

//WaitForMessage ...
func WaitForMessage(client RedisConnection) (messageString string, err error) {

	messages := make(chan radix.PubSubMessage, 1)
	errCh := make(chan error, 1)

	client.pubsubClient.Subscribe(messages, redisTopic)
	select {
	case msg := <-messages:
		log.Printf("publish to channel %q received: %q\n", msg.Channel, msg.Message)

		messageString = string(msg.Message)
	case err := <-errCh:
		log.Printf("Error Received : %q\n", err)
	}

	client.pubsubClient.Unsubscribe(messages, redisTopic)
	return

}

//PublishMessage ...
func PublishMessage(client RedisConnection, messageString string) (err error) {

	//messages := make(chan radix.PubSubMessage, 1)
	//errCh := make(chan error, 1)

	//client.pubsubClient.Subscribe(messages, redisTopic)

	// /messages <- radix.PubSubMessage{Message: []byte(messageString)}

	client.redisClient.Do(radix.Cmd(nil, "PUBLISH", redisTopic, messageString))

	//time.Sleep(2)

	//client.pubsubClient.Unsubscribe(messages, redisTopic)
	return

}
