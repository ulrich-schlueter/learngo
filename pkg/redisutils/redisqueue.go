package redisutils

import (
	"encoding/json"
	"fmt"

	radix "github.com/mediocregopher/radix/v3"
	"gitlab.com/ulrich-schlueter/learngo/pkg/mandel"
)

type queueItem struct {
	MandelBox      mandel.BoundingBox
	PictureDetails mandel.PictureDetails
}

var redisqueueName = "Mandelqueue"

//ClearQueue ...
func ClearQueue(client RedisConnection) (err error) {
	err = client.redisClient.Do(radix.Cmd(nil, "DEL", redisqueueName))
	if err != nil {
		fmt.Println("Error Deleting key " + err.Error())
	}
	return
}

//AddItemToQueue ...
func AddItemToQueue(client RedisConnection, m mandel.BoundingBox, p mandel.PictureDetails) (err error) {

	qi := queueItem{MandelBox: m, PictureDetails: p}

	b, _ := json.Marshal(qi)
	bstring := string(b)

	err = client.redisClient.Do(radix.Cmd(nil, "RPUSH", redisqueueName, bstring))
	if err != nil {
		fmt.Println("Error Queuing Item  " + err.Error())
	}
	return
}

//GetItemFromQueue ...
func GetItemFromQueue(client RedisConnection) (m mandel.BoundingBox, p mandel.PictureDetails, err error) {

	var qi queueItem

	var jsonData []string
	err = client.redisClient.Do(radix.Cmd(&jsonData, "BLPOP", redisqueueName, "0"))
	if err != nil {
		fmt.Println("Error Queuing Item  " + err.Error())
		return
	}

	err = json.Unmarshal([]byte(jsonData[1]), &qi)
	if err != nil {
		fmt.Println("Error DeQueuing Item  " + err.Error())
	}
	m = qi.MandelBox
	p = qi.PictureDetails
	return
}
