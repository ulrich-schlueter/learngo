package redisutils

import (
	"fmt"
	"testing"

	"gitlab.com/ulrich-schlueter/learngo/pkg/mandel"
)

func TestList(t *testing.T) {
	client, err := testSetup()
	if err != nil {
		t.Errorf("Failed to build client: %s ", err.Error())
	}

	mIn := mandel.Mandel{CenterX: 0, CenterY: 1, Radius: 2, Iterations: 3}
	mJSON := "{\"CenterX\":0,\"CenterY\":1,\"Radius\":2,\"Iterations\":3}"

	err = ClearExampleList(client)
	if err != nil {
		t.Errorf("Failed to clear Example List: %s ", err.Error())
	}

	l, err := GetNumberOfExamples(client)
	if err != nil {
		t.Errorf("Failed to Get number of items in Example List: %s ", err.Error())
	}

	if l != 0 {
		t.Errorf("Example List not empty: Got %d expected: 0  ", l)
	}

	err = InjectExample(client, mIn)
	if err != nil {
		t.Errorf("Failed to add item  %s ", err.Error())
	}

	list, err := GetExampleParamList(client)
	if err != nil {
		t.Errorf("Failed to retrieve list  %s ", err.Error())
	}

	if len(list) != 1 {
		t.Errorf("Unexpected List Length: Got %d expected 1", len(list))
	}

	if list[0] != mJSON {
		t.Errorf("Unexpected Item: Got %s expected %s", list[0], mJSON)
	}

}

func TestPubSub(t *testing.T) {
	client, err := testSetup()
	if err != nil {
		t.Errorf("Failed to build client: %s ", err.Error())
	}

	sendMessage := "TestMessage"

	go func() {
		fmt.Println("Publishing")
		err = PublishMessage(client, sendMessage)
		if err != nil {
			t.Errorf("Failed to publish message: %s ", err.Error())
		}

		fmt.Println("Sent", sendMessage)
	}()

	fmt.Println("Receiving")
	receivedMessage, err := WaitForMessage(client)
	if err != nil {
		t.Errorf("Failed to retrieve message: %s ", err.Error())
	}
	if sendMessage != receivedMessage {
		t.Errorf("Unexpected Message received: Wanted %s  Got %s ", sendMessage, receivedMessage)
	}
	fmt.Println("Received", receivedMessage)

	fmt.Println("Waiting to converge")

	fmt.Println("Test finished")
}
