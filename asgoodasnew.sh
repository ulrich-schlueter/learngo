#!/bin/bash
#set -x

PARAM=$1

X=$(pwd)

oc delete -f deploy/j7first-deployment.yaml


if [ "$PARAM" == 'full' ] 
then
    oc delete -f deploy/j7first-rediscluster.yaml

    for pv in  redis-master-redis-master-0 redis-master-redis-master-1 redis-master-redis-master-2 
    do
        oc delete pvc/$pv
    done


    for pv in  redis-slave-redis-slave-0 redis-slave-redis-slave-1 redis-slave-redis-slave-2 
    do
    oc delete pvc/$pv
    done

    oc apply -f deploy/j7first-rediscluster.yaml


    
    STATUS=$(oc get po redis-slave-2 -o template --template '{{.status.phase}}')
    while [ "$STATUS" != "Running" ]
    do
        sleep 5
        STATUS=$(oc get po redis-slave-2 -o template --template '{{.status.phase}}')

    done

    ISCLUSTER=$(oc logs po/redis-master-0 redis-master | grep "Cluster state changed: ok" | wc -l)
    while [ "$ISCLUSTER" != "1" ]
    do
        sleep 5
        ISCLUSTER=$(oc logs po/redis-master-0 redis-master | grep "Cluster state changed: ok" | wc -l)

    done



    sleep 15
fi


cd tools/exampledataimport/deploy

VERSION=$(docker images learngo-datainjector --format "{{.Tag}}"  | head -n1)
ls
./deployMe.sh $VERSION
cd $X
oc apply -f deploy/j7first-deployment.yaml
